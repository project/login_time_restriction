<?php

namespace Drupal\login_time_restriction\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\user\UserDataInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event Subscriber ValidRequestEventSubscriber.
 */
class ValidRequestEventSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Entity type manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The request stacks service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The constructor method.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack service.
   * @param \Drupal\user\UserDataInterface $user_data
   *   The user data service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(AccountInterface $current_user, EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, RequestStack $request_stack, UserDataInterface $user_data, ConfigFactoryInterface $config_factory) {
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->requestStack = $request_stack;
    $this->userData = $user_data;
    $this->configFactory = $config_factory;
  }

  /**
   * Request Event to check if the user is having the valid access permission.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   Event.
   */
  public function onUserRequest(ResponseEvent $event) {
    // Current logged in user object.
    $user_info = [];
    if ($this->currentUser->id() > 0 && $this->configFactory->get('login_time_restriction.settings')->get('enable')) {
      $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
      $user_info = _login_time_restriction_get_user_info($user);
      // Validate if the user is accessing site in allowed time.
      $current_time = time();
      if (array_key_exists('start_time', $user_info) && array_key_exists('end_time', $user_info)) {
        if (!($current_time >= $user_info['start_time'] && $current_time <= $user_info['end_time'])) {
          // Logout user from the site.
          user_logout();
          $response = new RedirectResponse(Url::fromRoute('user.login', ['access_logout' => 1])->toString());
          $event->setResponse($response);
        }
        else {
          // Add attachments if the request is still valid.
          $response = $event->getResponse();
          if ($response instanceof HtmlResponse) {
            $user_info['show_warning_message'] = $this->userData->get('login_time_restriction', $this->currentUser->id(), 'show_access_time');
            $attachments = $response->getAttachments();
            $attachments['drupalSettings']['user']['ltr'] = $user_info;
            $response->setAttachments($attachments);

            return $response;
          }
        }
      }
    }
    else {
      $access_logout = $this->requestStack->getCurrentRequest()->query->get('access_logout');
      $post = $this->requestStack->getCurrentRequest()->request->all();
      if (!empty($access_logout) && $access_logout == 1 && empty($post)) {
        $this->messenger->addError($this->configFactory->get('login_time_restriction.settings')->get('message'));
      }
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onUserRequest', 5];

    return $events;
  }

}

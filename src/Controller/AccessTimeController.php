<?php

namespace Drupal\login_time_restriction\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\user\UserData;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Marks the show_access_limit flag as confirmed.
 */
class AccessTimeController extends ControllerBase {

  /**
   * Entity type manager object.
   *
   * @var \useDrupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Constructs an AccessTimeController object.
   *
   * @param \Drupal\user\UserData $userData
   *   Data of the user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   */
  public function __construct(
    UserData $userData,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    $this->userData = $userData;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.data'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Updates the Userdata API on confirmation.
   */
  public function confirmWarningMessage() {
    $current_user_id = $this->currentUser()->id();
    $this->userData->set('login_time_restriction', $current_user_id, 'show_access_time', FALSE);
    // Invalidate cache tags after user data is updated.
    $tags = $this->entityTypeManager->getStorage('user')->load($current_user_id)->getCacheTags();
    if ($tags) {
      Cache::invalidateTags($tags);
    }

    return [];
  }

  /**
   * Logout user if time expires.
   */
  public function confirmLogout() {
    // Terminate the current user session.
    user_logout();
    return new RedirectResponse(Url::fromRoute('user.login', ['access_logout' => 1])->toString());
  }

}

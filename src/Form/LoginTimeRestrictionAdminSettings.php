<?php

namespace Drupal\login_time_restriction\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\login_time_restriction\Facade\TimeRangeFacade;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * LoginTimeRestrictionAdminSettings to get the access time configuration.
 */
class LoginTimeRestrictionAdminSettings extends ConfigFormBase {

  /**
   * Entity type manager object.
   *
   * @var \useDrupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The class resolver to get the subform form objects.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * The constructor method.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver to get the subform form objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, ClassResolverInterface $class_resolver) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->classResolver = $class_resolver;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // Load the service required to construct this class.
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('class_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['login_time_restriction.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'login_time_restriction_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('login_time_restriction.settings');

    $form['login_time_restriction'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Login Time Restriction Configuration'),
    ];

    $form['login_time_restriction']['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable login time restriction'),
      '#description' => $this->t('When enabled, The time based login restriction will be applied.'),
      '#default_value' => $config->get('enable'),
    ];

    $form['login_time_restriction']['time_based_login_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable per day login restriction'),
      '#description' => $this->t('When enabled, The time based login restriction will be applied on the daily basis. NOTE: This will change the field_ltr_access_time configuration.'),
      '#default_value' => $config->get('time_based_login_enable'),
    ];

    $form['login_time_restriction']['message'] = [
      '#type' => 'textfield',
      '#size' => 255,
      '#title' => $this->t('Error message'),
      '#description' => $this->t('Message that will be shown to the user at the time of login.'),
      '#default_value' => $config->get('message'),
    ];

    $form['login_time_restriction']['warning_time'] = [
      '#type' => 'number',
      '#title' => $this->t('Warning Time ( in mins )'),
      '#description' => $this->t('Based on this, The user will be notified about the session termation. Ex: if you add 15 mins here, then user will be notified 15 mins before the session termation via popup.'),
      '#default_value' => $config->get('warning_time'),
    ];

    $form['login_time_restriction']['show_sticky_timer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show sticky timer'),
      '#description' => $this->t('When enabled, A sticky timer will be shown to the users.'),
      '#default_value' => $config->get('show_sticky_timer'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('login_time_restriction.settings');
    // Check if the value of time_based_login_enable is changed.
    if ($config->get('time_based_login_enable') != $form_state->getValue('time_based_login_enable')) {
      $type = $form_state->getValue('time_based_login_enable') ? 'time_range' : 'daterange_default';
      $field_config = $this->entityTypeManager->getStorage('field_config')->load('user.user.field_ltr_access_time');
      // Modify the widget via Facade.
      $this->classResolver->getInstanceFromDefinition(TimeRangeFacade::class)->changeWidget($field_config, $type);
    }
    $config->clear('login_time_restriction');
    // Storing the config values.
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();
    $ignored_values = [
      'submit',
      'form_build_id',
      'form_token',
      'form_id',
      'op',
      'actions',
    ];
    foreach ($values as $name => $value) {
      if (!in_array($name, $ignored_values)) {
        if (isset($value['value'])) {
          $value = $value['value'];
        }

        $config->set($name, $value);
      }
    }
    $config->save();
  }

}

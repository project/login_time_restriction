<?php

namespace Drupal\login_time_restriction\Facade;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigInstallerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\field\FieldConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a facade for integrating with Time Range.
 *
 * @internal
 *   This is a totally internal part of Login Time Restriction and may be
 *   changed in any way, or removed outright, at any time without warning.
 *   External code should not use this class!
 */
final class TimeRangeFacade implements ContainerInjectionInterface {

  /**
   * The config installer service.
   *
   * @var \Drupal\Core\Config\ConfigInstallerInterface
   */
  protected $configInstaller;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entity type manager object.
   *
   * @var \useDrupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * MetatagFacade constructor.
   *
   * @param \Drupal\Core\Config\ConfigInstallerInterface $config_installer
   *   The config installer service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(ConfigInstallerInterface $config_installer, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository) {
    $this->configInstaller = $config_installer;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.installer'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * Executed when new daterange field is imported.
   *
   * Modify the Widget of field_ltr_access_time based on the type specified by
   * 'login_time_restriction.field_widget' third-party setting.
   *
   * @param \Drupal\field\FieldConfigInterface $field_config
   *   The new field's config definition.
   * @param string $widget_type
   *   The machine name of widget that you want to replace with.
   */
  public function changeWidget(FieldConfigInterface $field_config, string $widget_type) {
    // We don't want to do any secondary config writes during a config sync,
    // since that can have major, unintentional side effects.
    // Proceed only if field_widget third party is present.
    if ($this->configInstaller->isSyncing() || empty($widget_type)) {
      return;
    }

    $target_entity = $field_config->getTargetEntityTypeId();
    $target_bundle = $field_config->getTargetBundle();

    // Get selected view modes for bundle.
    $view_modes = $this->entityDisplayRepository
      ->getViewModeOptionsByBundle(
        $target_entity, $target_bundle
      );

    // Get format settings for field.
    foreach (array_keys($view_modes) as $view_mode) {
      $settings = $this->entityTypeManager
        ->getStorage('entity_form_display')
        ->load($target_entity . '.' . $target_bundle . '.' . $view_mode);
      // Check if this view mode is available or not.
      if (is_object($settings)) {
        $settings->setComponent($field_config->getName(), [
          'type' => $widget_type,
          'settings' => [],
        ])->save();
      }
    }
  }

}

(function($, Drupal, drupalSettings) {

  Drupal.behaviors.login_time_restriction = {
    attach: function (context, settings) {
      if (context !== document) {
        return;
      }
      // Distructure the given user settings.
      if (settings.user.ltr && settings.ltr) {
        const { start_time, end_time, user_time_zone, show_warning_message } = settings.user.ltr;
        const { enabled, warning_time, show_sticky_timer } = settings.ltr;
        if (start_time && end_time && enabled) {
          const converted_time = new Date(new Date().toLocaleString("en-US", {timeZone: user_time_zone}));
          const current_time = Math.round((new Date(converted_time).getTime() / 1000));
          const diff = end_time - current_time;
          // Do not proceed if diff is less than 0.
          if (diff <= 0) {
            // Logout the user.
            ajaxLogout();
          } else {
            // setTimeout to display the warning before the warning time.
            if (show_warning_message) {
              setTimeout(function() {
                $.fn.warningDialog(
                  'ltr-warning-message',
                  Drupal.t('Your session is about to terminate. The session end time is @time.', {'@time': new Date(end_time*1000)}),
                  Drupal.t('Warning'),
                  'warning'
                );
              }, (diff - warning_time * 60 ) * 1000);
            }
            // setTimeout to trigger logout after the time is finished.
            if (warning_time) {
              setTimeout(function() {
                ajaxLogout();
              }, diff * 1000);
            }
            // Show sticky timer.
            const id = 'ltr-sticky-timer';
            if (show_sticky_timer && !document.getElementById(id)) {
              const element = document.createElement('div');
              element.id = id;
              const countDownDate = new Date(end_time*1000).getTime();
              // Prepare the time in the desired format.
              // Update the count down every 1 second
              var x = setInterval(function () {
                // Get today's date and time
                var now = new Date().getTime();

                // Find the distance between now and the count down date
                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Display the result in the element with id="demo"
                element.innerHTML = days + "d " + hours + "h "
                  + minutes + "m " + seconds + "s ";

                // If the count down is finished, write some text
                if (distance < 0) {
                  clearInterval(x);
                  element.innerHTML = "EXPIRED";
                }
              }, 1000);
              document.body.appendChild(element);
            }
          }
        }
      }
    }
  }
  function ajaxLogout() {
    $.ajax({
      url: drupalSettings.path.baseUrl + 'ltr_confirm_logout',
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('X-Requested-With', {
          toString: function () {
            return '';
          }
        });
      },
      success: function() {
        window.location = drupalSettings.path.baseUrl + 'user/login?access_logout=1';
      }
    })
  }
  // Warning dialog.
  $.fn.warningDialog = function(id, message, title, type) {
    Drupal.dialog(
      '<div id="' + id + '" class="ltr-dialog">' + message + '</div>',
      {
        title: title,
        resizable: false,
        buttons: [
          {
            text: Drupal.t('Ok'),
            click: function() {
              $(this).dialog("close").remove();
            }
          },
        ],
        closeOnEscape: true,
        classes: {
          'ui-dialog': id + '-dialog dialog-type-' + type,
        },
        close: function() {
          // Set the status of warning message as false.
          $.ajax({
            url: drupalSettings.path.baseUrl + 'ltr_access-time-confirmed',
            type: 'GET',
          })
        }
      }
    ).showModal();
  }

}(jQuery, Drupal, drupalSettings));

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Want to restrict the access of users on the basis of time? If 'YES' then you're
at the right place. The basic idea behind this module is to restrict user access
to the site based on time.
Eg: If you want to allow a user to access the site starting from 10:00 AM to
12:00 PM then you can easily do it using this module.


REQUIREMENTS
------------

This module requires the following modules:

 * Time Range (https://www.drupal.org/project/time_range)


RECOMMENDED MODULES
-------------------

No recommended modules.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

 * As soon as you install the module, you will be able to see a new field in
   user edit profile named `Access Time`. Here you can configure the timing for
   the individual users.

 * Visit the login_time_restriction settings form at
   (/admin/login_time_restriction/settings) or `Configuration >>
   Login time restriction settings.`

 * The configuration option that is available with this module are following:
  ```
    1. Enable login time restriction: Enable the module.
    2. Enable per day login restriction: Enable the per day login restriction
       ( The user will be allowed to login on per day basis, Like if we allow
       user from 10:00 AM to 10:00 PM then the user will be allowed to login
       everyday in this timerange.)
    3. Error message: Add the error message that will be shown to the user, if
       they try to login into the site outside of the allowed timerange.
    4. Warning Time (in mins): Display a warning message before the mentioned
       time saying that the session is about to terminate. Ex: If Warning time
       is 15 then the user will get a warning popup before 15 mins of the
       session termination. ( The popup will only appear once per session - Only
       if the user excepts the popup )
  ```


MAINTAINERS
-----------

Current maintainers:
 * Kunal Singh - https://www.drupal.org/u/kunal_singh

<?php

namespace Drupal\Tests\login_time_restriction\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the settings of the module.
 *
 * @group login_time_restriction`
 */
class AccessTimeSettingsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'login_time_restriction',
    'field_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $field_config = $this->container->get('entity_type.manager')->getStorage('field_config')->load('user.user.field_ltr_access_time');
    $type = $field_config->getThirdPartySetting('login_time_restriction', 'field_widget', '');
    $target_bundle = $field_config->getTargetBundle();

    $this->container->get('entity_display.repository')
      ->getFormDisplay('user', $target_bundle)
      ->setComponent($field_config->getName(), [
        'type' => $type,
        'settings' => [],
      ])->save();
  }

  /**
   * Tests the settings form to ensure the correct default values are used.
   */
  public function testSettingsPage() {
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->rootUser);
    // Check if time_based_login is changing the widget type of access time
    // field.
    $id = $this->rootUser->id();
    $this->drupalGet("/user/$id/edit");
    $assert_session->statusCodeEquals(200);
    $assert_session->fieldExists('field_ltr_access_time[0][value][time]');
    $assert_session->fieldExists('field_ltr_access_time[0][end_value][time]');

    $this->drupalGet('/admin/login_time_restriction/settings');
    $assert_session->statusCodeEquals(200);

    // Get the default value and check if it's getting used.
    $default_value = $this->config('login_time_restriction.settings')->getRawData();
    // Unset _core as it's not the part of settings config.
    unset($default_value['_core']);
    foreach ($default_value as $name => $value) {
      $assert_session->fieldValueEquals($name, $value);
    }
    // Chaning value and checking if values are getting saved.
    $edit = [
      'enable' => TRUE,
      'time_based_login_enable' => FALSE,
      'message' => 'This is the test message',
      'warning_time' => 10,
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    // Check that settings have the correct default value.
    foreach ($edit as $name => $value) {
      $assert_session->fieldValueEquals($name, $value);
    }
  }

}
